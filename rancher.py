import subprocess
import re
import toml
import argparse
import sqlite3
from datetime import date
from tabulate import tabulate

conn = sqlite3.connect("r1_history.db")
cur = conn.cursor()


def crun_setup():
    ##########
    # CREATE #
    ##########
    cur = conn.cursor()
    cur.execute('''
        CREATE TABLE IF NOT EXISTS history(
            id INTEGER PRIMARY KEY,
            rancher_instance TEXT,
            totalcontainers INT,
            date TEXT
        )
    ''')
    conn.commit()


def crun_insert(instance, output, date):
    cur.execute('''INSERT INTO history(rancher_instance, totalcontainers, date)
                    VALUES(?,?,?)''', (instance, output, date))
    conn.commit()


def crun_read():
    cur.execute("SELECT rancher_instance, totalcontainers, date FROM history")
    return(cur.fetchall())


def crun_delete():
    cur.execute("DELETE FROM history")
    conn.commit()
    return(cur.fetchall())


def ranchercli(accesskey, secretkey, renv, address):  # Define function
    # Command setup to run rancher cli binary
    command = f"docker run -t --rm rancherqa /opt/rancher --url {address} --env {renv} --access-key {accesskey} -secret-key {secretkey} ps -c | sort -k 3 | awk '!seen[$3]++' " + "| awk '{print $3}'"  # noqa: E501
    # Run process and convert byte output format to UTF8, strip any
    # extra whitespace and split by newlines (\n)
    rawdata = (subprocess.check_output(command, shell=True, encoding='UTF-8').strip().splitlines())  # noqa: E501
    # Return array of output and sort from A-Z
    print()
    return sorted(rawdata)


def clean_field(field):
    field = re.sub(r'^.*?/', '', field)
    return field


def compare_two(Output, Output2):
    # Get both image lists
    # Function resuse
    outputtable = []
    # For every image strip registry from path and compare every
    # image with the other specified enviroment.
    for dataobjects in Output:
        dataobjects = clean_field(dataobjects)
        for dataobjects2 in Output2:
            dataobjects2 = clean_field(dataobjects2)
            if(dataobjects.split(':')[0] == dataobjects2.split(':')[0]):
                if(dataobjects != dataobjects2):
                    # Append to table ready for output
                    outputtable.append([dataobjects, dataobjects2])
                    # print( [ dataobjects, '   ->  ', dataobjects2 ])
    return outputtable


def list_one(Output):
    # List all containers
    #   # noqa: E501
    list1 = []
    args = get_arguments()
    for dataobjects in Output:
        dataobjects = clean_field(dataobjects)
        list1.append(dataobjects)
        print(dataobjects)
    crun_insert(args.instance, (len(list1)), date.today())


def get_arguments():
    # Argument parser
    parser = argparse.ArgumentParser(description='Rancher1 container compare tool')  # noqa: E501
    # Set arguments
    parser.add_argument("instance", help='Instance to use eg: iom-kentech')
    parser.add_argument('-c', '--compare', help='Compare with enviroment', required=False)  # noqa: E501
    # Parse arguments
    return parser.parse_args()


def get_rancher_config(config):
    return ranchercli(config["accessKey"], config["secretKey"], config["rancher_env"], config["address"])  # noqa: E501


def main():
    crun_setup()
    args = get_arguments()
    config_path = "config.toml"
    config = toml.load(config_path)

    # If user is running with history argument
    if args.instance == "history":
        print(crun_read())

    elif args.instance == "clear":
        (crun_delete())
        print("History deleted")

    # If user is running with -c flag
    elif args.compare is not None:
        Output = get_rancher_config(config[args.instance])
        Output2 = get_rancher_config(config[args.compare])
        outputtable = compare_two(Output, Output2)
        print(tabulate(outputtable, headers=[args.instance, args.compare], tablefmt='orgtbl'))  # noqa: E501
    else:
        Output = get_rancher_config(config[args.instance])
        list_one(Output)
