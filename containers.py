import subprocess
import re
import toml
import argparse
from tabulate import tabulate

# Argument parser
parser = argparse.ArgumentParser(description='Rancher1 container compare tool')
# Set arguments
parser.add_argument("instance", help='Instance to use eg: iom-kentech')
parser.add_argument('-c', '--compare', help='Compare with enviroment', required=False)  # noqa: E501
# Parse arguments
args = parser.parse_args()

# define table for output when two rancher instances used
outputtable = []

# Load toml configuration.
config = toml.load("config.toml")


def ranchercli(accesskey, secretkey, renv, address):  # Define function
    # Command setup to run rancher cli binary
    command = f"/Users/darrenleatherbarrow/Downloads/rancher-v0.6.12/rancher --url {address} --env {renv} --access-key {accesskey} -secret-key {secretkey} ps -c | sort -k 3 | awk '!seen[$3]++' " + "| awk '{print $3}'"  # noqa: E501
    # Run process and convert byte output format to UTF8, strip any
    # extra whitespace and split by newlines (\n)
    rawdata = (subprocess.check_output(command, shell=True, encoding='UTF-8').strip().splitlines())  # noqa: E501
    # Return array of output and sort from A-Z
    return sorted(rawdata)


# If user is running with -c flag
if args.compare is not None:
    Differences = []
    # Get both image lists
    Output = ranchercli(config[args.instance]["accessKey"], config[args.instance]["secretKey"], config[args.instance]["rancher_env"], config[args.instance]["address"])  # noqa: E501
    Output2 = ranchercli(config[args.compare]["accessKey"], config[args.compare]["secretKey"], config[args.compare]["rancher_env"], config[args.compare]["address"])  # noqa: E501
    # For every image strip registry from path and compare every
    # image with the other specified enviroment.
    for dataobjects in Output:
        dataobjects = re.sub(r'^.*?/', '', dataobjects)
        for dataobjects2 in Output2:
            dataobjects2 = re.sub(r'^.*?/', '', dataobjects2)
            if(dataobjects.split(':')[0] == dataobjects2.split(':')[0]):
                if(dataobjects != dataobjects2):
                    # Append to table ready for output
                    outputtable.append([dataobjects, dataobjects2])
                    # print( [ dataobjects, '   ->  ', dataobjects2 ])
    print(tabulate(outputtable, headers=[args.instance, args.compare], tablefmt='orgtbl'))  # noqa: E501
else:
    # List all containers
    Output = ranchercli(config[args.instance]["accessKey"], config[args.instance]["secretKey"], config[args.instance]["rancher_env"], config[args.instance]["address"])  # noqa: E501
    for dataobjects in Output:
        dataobjects = re.sub(r'^.*?/', '', dataobjects)
        print(dataobjects)
