import rancher

sample_container = 'registry.sp.com:4567/sportpesa/global/old-system-wrapper:2.2.0'  # noqa: E501
sample_container1 = 'registry.sp.com:4567/sportpesa/global/memcached:3'
stripped_container = 'sportpesa/global/old-system-wrapper:2.2.0'
array = ['IMAGE', 'docker.elastic.co/elasticsearch/elasticsearch:6.4.0', 'parser:2.4.6']  # noqa: E501
array2 = ['IMAGE', 'docker.elastic.co/elasticsearch/elasticsearch:6.5.0', 'parser:2.4.6']  # noqa: E501


def test_fieldremove():
    # Is registry stripped from input.
    assert rancher.clean_field(sample_container) == stripped_container


def test_diff():
    # Check differences are detected
    assert len(rancher.compare_two(array, array2)) == 1
    # Check differences no differences when match
    assert len(rancher.compare_two(array, array)) == 0

#  def test_list_containers():
#    assert  "IMAGE" in rancher.list_one(array)
